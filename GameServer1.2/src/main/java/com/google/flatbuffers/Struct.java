/*
 * Copyright (c) 2015.
 * 游戏服务器核心代码编写人石头哥哥拥有使用权
 * 最终使用解释权归创心科技所有
 * 联系方式：E-mail:13638363871@163.com ;
 * 个人博客主页：http://my.oschina.net/chenleijava
 * powered by 石头哥哥
 */

package com.google.flatbuffers;

import java.nio.ByteBuffer;

// All structs in the generated code derive from this class, and add their own accessors.
public class Struct {
  protected int bb_pos;
  protected ByteBuffer bb;
}
