/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit;

import org.apache.commons.lang.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	/**默认开始时间*/
	public final static String DEFAULT_STARTTIME = "2009-01-01";
	/**默认结束时间*/
	public final static String DEFAULT_ENDTIME = "2019-01-01";

	public final static SimpleDateFormat DATEFORMAT_DAY = new SimpleDateFormat("yyyy-MM-dd");
	
	public final static SimpleDateFormat DATEFORMAT_SEC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * 取开始时间
	 * @param startTime
	 * @return
	 * @throws Exception
	 */
	public static Date getStartTime(String startTime) throws Exception{
		if(StringUtils.isEmpty(startTime)){
			startTime = DEFAULT_STARTTIME;
		}
		return DateUtil.DATEFORMAT_DAY.parse(startTime);
	}
	/**
	 * 取结束时间
	 * @param endTime
	 * @return
	 * @throws Exception
	 */
	public static Date getEndTime(String endTime) throws Exception{
		if(StringUtils.isEmpty(endTime)){
			endTime = DEFAULT_ENDTIME;
		}
		return DateUtil.DATEFORMAT_DAY.parse(endTime);
	}
	/**
	 * 计算刷新天数
	 * 5点刷新
	 * @param lastTime
	 * @param nowTime
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static int getDay(Timestamp lastTime, Timestamp nowTime) {
		if(nowTime == null)
			nowTime = new Timestamp(System.currentTimeMillis());
		if(lastTime == null )
			return 1;
		if (lastTime.getHours() < 5) {
			// 1-3情况
			if (nowTime.getHours() < 5) {
                return getDiffDay(nowTime,lastTime);
			// 1-4情况
			} else {
				return getDiffDay(nowTime,lastTime) + 1;
			}
		} else {
			//2-3情况
			if (nowTime.getHours() < 5) {
				return getDiffDay(nowTime,lastTime) - 1;
		    //2-4情况
			} else {
				return getDiffDay(nowTime,lastTime);
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public static int getDiffDay(Timestamp time1, Timestamp time2){
		return getDay1(new Date(time2.getTime()),new Date(time1.getTime()));
	}
	
	/**
	 * 计算是否可获得免费令
	 * 8点半 12点半 19点半 21点半
	 * @param lastTime
	 * @param nowTime
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static boolean getFreeActPoint(Timestamp lastTime, Timestamp nowTime){
		int lastTimeCount = lastTime.getHours() * 60 + lastTime.getMinutes();
		int nowTimeCount = nowTime.getHours() * 60 + nowTime.getMinutes();
		int count1 = 8 * 60 + 30;
		int count2 = 12 * 60 + 30;
		int count3 = 19 * 60 + 30;
		//同属于一天
		if(lastTime.getDate() == nowTime.getDate()){
			if(lastTimeCount >= count3){
				return false;
			}else if(lastTimeCount < count1)
				return nowTimeCount >= count1;
			else if(lastTimeCount >= count1 && lastTimeCount < count2){
				return nowTimeCount >= count2;
			}else if(lastTimeCount >= count2 && lastTimeCount < count3){
				return nowTimeCount >= count3;
			}
		}else{
			if(lastTimeCount >= count3){
				return nowTimeCount >= count1;
			}
		}
		return true;
	}
	
	/**
	 * 0点刷新
	 * @param d1 最后执行时间
	 * @param d2 当前时间
	 * @return 
	 */
	@SuppressWarnings("deprecation")
	public static int getDay1(Date d1,Date d2){
		if(d1 == null)
			return 1;
		d1.setHours(0);
		d1.setMinutes(0);
		d1.setSeconds(0);
		d2.setHours(0);
		d2.setMinutes(0);
		d2.setSeconds(0);
		long   date1   =   d1.getTime()   /   (1000l*60*60*24);
		long   date2   =   d2.getTime()   /   (1000l*60*60*24); 
		return Long.valueOf(date2 - date1).intValue();
	}
	
	public static void main(String args[]) throws ParseException{
		//Date date1 = new Date(playerDailyTaskModel.getCreateTime().getTime());
		Calendar cal1 = Calendar.getInstance();
	    Calendar cal2 = Calendar.getInstance();
	    cal1.setTime(new Date(Timestamp.valueOf("2012-04-15 20:00:00").getTime()));
	    cal2.setTime(new Date(Timestamp.valueOf("2012-04-16 12:00:00").getTime()));
	    System.out.println("cal1.WEEK_OF_YEAR=:" + cal1.get(Calendar.WEEK_OF_YEAR) );
	    System.out.println("cal2.WEEK_OF_YEAR=:" + cal2.get(Calendar.WEEK_OF_YEAR) );

		
	}
}


