/*
 * Copyright (c) 2015.
 * 游戏服务器核心代码编写人石头哥哥拥有使用权
 * 最终使用解释权归创心科技所有
 * 联系方式：E-mail:13638363871@163.com ;
 * 个人博客主页：http://my.oschina.net/chenleijava
 * powered by 石头哥哥
 */

package com.net;


import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;

/**
 * @author 石头哥哥
 *         Date: 13-11-27</br>
 *         Time: 上午11:20</br>
 *         Package: com.dc.gameserver.baseConfig</br>
 *         注解：游戏服务器配置文件加载
 */
public class Config {

    /**
     * 加载游戏配置
     *
     * @throws ConfigurationException
     */
    public static void IntiConfig() throws ConfigurationException {

        PropertiesConfiguration server_config = new PropertiesConfiguration(DEFAULT_VALUE.FILE_PATH.SERVER);

        server_config.setEncoding("UTF-8");
        boolean autoReload = server_config.getBoolean("autoReload");
        if (autoReload) {
            FileChangedReloadingStrategy server_strategy = new FileChangedReloadingStrategy();                    //default 5000
            FileChangedReloadingStrategy game_strategy = new FileChangedReloadingStrategy();                    //default 5000

            int checkTime = server_config.getInt("refreshDelay", 5000);
            server_strategy.setRefreshDelay(checkTime);
            server_config.setReloadingStrategy(server_strategy);
            game_strategy.setRefreshDelay(checkTime);
        }


        /*********************************************************************************/
        //   加载游戏底层基础配置
        /*********************************************************************************/
        DEFAULT_VALUE.SERVER_VALUE.adminServerHost = server_config.getString("adminServerHost");
        DEFAULT_VALUE.SERVER_VALUE.adminServerPort = server_config.getInt("adminServerPort");
        DEFAULT_VALUE.SERVER_VALUE.redisShared = server_config.getBoolean("redisShared");
        DEFAULT_VALUE.SERVER_VALUE.nativeEpoll = server_config.getBoolean("nativeEpoll");

        DEFAULT_VALUE.SERVER_VALUE.gameserverPort = server_config.getInt("gameserverPort", 8888);
        DEFAULT_VALUE.SERVER_VALUE.readTimeOut = server_config.getInt("readTimeOut");
        DEFAULT_VALUE.SERVER_VALUE.writeTimeOut = server_config.getInt("writeTimeOut");
        DEFAULT_VALUE.SERVER_VALUE.connect_timeout = server_config.getInt("connect_timeout");
        DEFAULT_VALUE.SERVER_VALUE.maxConnection = server_config.getInt("maxConnection");
        DEFAULT_VALUE.SERVER_VALUE.checkInterval = server_config.getInt("checkInterval");

        //传输协议参数配置
        DEFAULT_VALUE.SERVER_VALUE.maxFrameLength = server_config.getInt("maxFrameLength", 1048576);
        DEFAULT_VALUE.SERVER_VALUE.lengthFieldOffset = server_config.getInt("lengthFieldOffset", 0);
        DEFAULT_VALUE.SERVER_VALUE.lengthFieldLength = server_config.getInt("lengthFieldLength", 4);
        DEFAULT_VALUE.SERVER_VALUE.lengthAdjustment = server_config.getInt("lengthAdjustment", 0);
        DEFAULT_VALUE.SERVER_VALUE.initialBytesToStrip = server_config.getInt("initialBytesToStrip", 4);

        //netty 中底层的配置参数
        DEFAULT_VALUE.SERVER_VALUE.recyclerMaxCapacity = server_config.getString("recyclerMaxCapacity", "1000");

        //底层服务线程池配置
        DEFAULT_VALUE.SERVER_VALUE.LogicThreads = server_config.getInt("LogicThreads");
        DEFAULT_VALUE.SERVER_VALUE.calculateThreads = server_config.getInt("calculateThreads");
        /*********************************************************************************
         加载游戏参数配置
         *********************************************************************************/
    }

    public static class DEFAULT_VALUE {
        /**
         * 文件配置路径
         */
        public static class FILE_PATH {

            /**
             * 游戏基础配置
             */
            public static String SERVER = "res/gameConfig/server.properties";
            public static String LOG4J = "res/gameConfig/log4j.properties";
            public static String SPRING_CONFIG_PATH = "res/springConfig/spring-context.xml";


            ////////////////////////////////////////////////////////////////////////


            /**
             * 技能基础属性配置文件
             */
            public static String GAME_XML_DATA_SKILL = "res/xmlConfig/skill.xml";

            /**
             *
             */
            public static String GAME_XML_DATA_PVP_REWARD []= {"res/xmlConfig/pvpReward.xml"};

            /**
             * 宝箱配置文件
             */
            public static String GAME_XML_DATA_CHEST = "res/xmlConfig/chest.xml";

            /**
             * 奇遇事件 基础配置文件
             */
            public static String GAME_XML_DATA_CHANGE = "res/xmlConfig/chance.xml";


            /**
             * 杂物
             */
            public static String GAME_XML_DATA_ITEM[] = {"res/xmlConfig/item.xml"};


            /**
             * 属性
             */
            public static String GAME_XML_DATA_PROPERTY_RANDOM_LEVEL[]
                    = {"res/xmlConfig/propertyRandomLevel.xml"};


            /**
             * 角色等级
             */
            public static String GAME_XML_DATA_ROLE_LEVEL[] = {"res/xmlConfig/roleLevel.xml"};

            /**
             * 商店数据源
             */
            public static String GAME_XML_DATA_SHOP[] = {"res/xmlConfig/shop.xml"};

            /**
             * 法宝
             */
            public static String GAME_XML_DATA_TRUMP[] = {"res/xmlConfig/trump.xml"};


            /**
             * 法宝随机
             */
            public static String GAME_XML_DATA_TRUMP_RANDOM[] = {"res/xmlConfig/trumpRandom.xml"};


            /**
             * 法宝技能随机
             */
            public static String GAME_XML_DATA_TRUMP_SKILL_RANDOM[] = {"res/xmlConfig/trumpSkillRandom.xml"};

            /**
             * 地图
             */
            public static String GAME_XML_DATA_SIMMPLE_MAP[] = {"res/xmlConfig/simpleMap.xml"};

            /**
             * 荣誉商店
             */
            public static String GAME_XML_DATA_HOR_SHOP[] = {"res/xmlConfig/honorShop.xml"};

            /**
             * 门派数据
             */
            public static String GAME_XML_DATA_FACTION[] = {"res/xmlConfig/faction.xml"};
            /**
             * 门派商店数据
             */
            public static String GAME_XML_DATA_FACTION_SHOP[] = {"res/xmlConfig/factionShop.xml"};
        }

        /**
         * 游戏基础配置
         */
        public static class GAME_VALUE {
        }

        /**
         * 游戏服务器底层配置
         */
        public static class SERVER_VALUE {

            public static boolean redisShared;
            //启用基于jni的native epoll
            public static boolean nativeEpoll;

            // #是否开启代理服务器
            public static boolean proxyServer;
            //#代理服务器端口
            public static int proxyServerPort;
            //  游戏服务器ip地址
            public static String remoteHost;
            public static int remotePort;


            //adminServer config
            public static String adminServerHost;
            public static int adminServerPort;


            //超时处理
            public static int readTimeOut = 0;//read data time out   ..secs
            public static int writeTimeOut = 0;//write data time out    ..secs
            public static int connect_timeout = 0;//connect_timeout SEC

            public static int gameserverPort = 0;//server port
            public static int maxConnection = 0;//最大连接数

            //数据包解析参数
            public static int maxFrameLength = 0;
            public static int lengthFieldOffset = 0;
            public static int lengthFieldLength = 0;
            public static int lengthAdjustment = 0;
            public static int initialBytesToStrip = 0;


            //netty 中底层的配置参数
            public static String recyclerMaxCapacity;


            //流量监控
            public static long checkInterval = 0;//监测流量间隔


            //////////////////////////线程池数量管理///////////////////////////////////////////////////////////////

            /**
             * 游戏任务检查线程池数量
             */
            public static int LogicThreads;
            /**
             * 游戏战斗逻辑计算线程数量(包含db操作部分)
             */
            public static int calculateThreads;


        }

    }

}
