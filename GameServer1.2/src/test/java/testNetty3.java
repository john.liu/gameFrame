/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */


import com.dc.gameserver.baseConfig.Config;
import com.net.NettyEngine3.core.NServerFrameNettyStart;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * @author 石头哥哥 </br>
 *         gameFrame </br>
 *         Date:14-2-18 </br>
 *         Time:下午6:08 </br>
 *         Package:{@link com.dc.gameserver}</br>
 *         Comment：
 */
public class testNetty3 {



    public static void main(String[]args){
        DOMConfigurator.configure(Config.DEFAULT_VALUE.FILE_PATH.LOG4J);
        new NServerFrameNettyStart().startServer();
    }

}
