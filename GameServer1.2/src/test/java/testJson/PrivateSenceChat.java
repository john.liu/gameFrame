/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package testJson;

/**
 * @author :陈磊 <br/>
 *         Project:CreazyGameServer1.6
 *         Date: 13-3-27
 *         Time: 下午7:02
 *         connectMethod:13638363871@163.com<br/>
 */
public class PrivateSenceChat {
    private byte roomtype;

    public byte getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(byte roomtype) {
        this.roomtype = roomtype;
    }


}
