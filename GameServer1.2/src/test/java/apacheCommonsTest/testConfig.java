/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package apacheCommonsTest;

import com.dc.gameserver.baseConfig.Config;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.xml.DOMConfigurator;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @author 石头哥哥 </br>
 *         Project : dcserver1.3</br>
 *         Date: 11/23/13  11</br>
 *         Time: 9:38 PM</br>
 *         Connect: 13638363871@163.com</br>
 *         packageName: apacheCommonsTest</br>
 *         注解： 应该废弃使用owner来加载配置文件
 */
public class testConfig {



    @Test
    public void config() throws ConfigurationException, FileNotFoundException, DocumentException {

        //xml config test
        DOMConfigurator.configureAndWatch("res/gameConfig/log4j.xml");
        XMLConfiguration goodsxml=new XMLConfiguration("res/goods.xml");
        HierarchicalConfiguration.Node node=goodsxml.getRoot();
        int count=node.getChild(0).getAttributeCount();  //节点元素
        String id= (String) node.getChild(0).getAttribute(3).getValue();

        SAXReader read = new SAXReader();
        Document doc = null;
        doc= read.read(new FileInputStream("res/goods.xml"));


        XMLConfiguration xmlConfiguration=new XMLConfiguration(Config.DEFAULT_VALUE.FILE_PATH.GAME_XML_DATA_LEVEL);
//        AttributeMap nodeList= (AttributeMap) xmlConfiguration.getDocument().getElementsByTagName("itemDrop");




//        Boolean auto = xmlConfiguration.getBoolean("basic.autoRelaodConfig",false) ;
//        long  refresh=xmlConfiguration.getLong("basic.refreshDelay");
//        System.out.println(auto);
//        FileChangedReloadingStrategy reloadingStrategy=new FileChangedReloadingStrategy();
//        reloadingStrategy.setRefreshDelay(xmlConfiguration.getLong("basic.refreshDelay"));
//        xmlConfiguration.setReloadingStrategy(reloadingStrategy);


        //properties config
        PropertiesConfiguration config = new PropertiesConfiguration("res/client.properties");
        FileChangedReloadingStrategy strategy=new FileChangedReloadingStrategy();                    //default 5000
        config.setReloadingStrategy(strategy);
        System.out.println(config.getInt("num"));
        System.out.println(config.getString("host","127.0.0.1"));


    }



}
